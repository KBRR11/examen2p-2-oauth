package pe.edu.upeu.academico.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="categoria")
public class Categoria implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idcategoria;
	private String nomcat;
	public Categoria() {
		super();
	}
	public Categoria(long idcategoria, String nomcat) {
		super();
		this.idcategoria = idcategoria;
		this.nomcat = nomcat;
	}
	public long getIdcategoria() {
		return idcategoria;
	}
	public void setIdcategoria(long idcategoria) {
		this.idcategoria = idcategoria;
	}
	public String getNomcat() {
		return nomcat;
	}
	public void setNomcat(String nomcat) {
		this.nomcat = nomcat;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}