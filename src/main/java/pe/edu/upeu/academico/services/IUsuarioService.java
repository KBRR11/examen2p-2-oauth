package pe.edu.upeu.academico.services;

import java.util.List;

import pe.edu.upeu.academico.entity.Usuario;


public interface IUsuarioService {

	public List<Usuario> findAll();
	public Usuario findById(Long iduser);
	public Usuario Add(Usuario user);
	public void Update(Usuario usuario,long idusuario);
	public void delete(Long iduser);
	public Usuario findByUsername(String username);
}
