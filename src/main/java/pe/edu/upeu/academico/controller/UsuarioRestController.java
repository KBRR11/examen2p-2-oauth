package pe.edu.upeu.academico.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.edu.upeu.academico.entity.Usuario;
import pe.edu.upeu.academico.services.IUsuarioService;



@RestController
@RequestMapping("/api")
public class UsuarioRestController {
@Autowired
private IUsuarioService usuarioService;

@GetMapping("/usuarios")
public List<Usuario> readAll(){
	return usuarioService.findAll();
}

@GetMapping("usuarios/{id}")
public Usuario findById(@PathVariable(name = "id") Long id) {
	return usuarioService.findById(id);
}

@PostMapping("/usuarios")
public Usuario Add(@RequestBody Usuario user) {
	return usuarioService.Add(user);
}

@PutMapping("/usuarios/{idusuario}")
public void update(@RequestBody Usuario usuario, @PathVariable(value = "idusuario") long idusuario) {
	usuarioService.Update(usuario, idusuario);
}

@DeleteMapping("/usuarios/{iduser}")
public void delete(@PathVariable(name = "iduser") Long iduser) {
	usuarioService.delete(iduser);
}

}
